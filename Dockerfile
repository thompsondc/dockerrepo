FROM tomcat:latest
ADD context.xml /usr/local/tomcat/webapps/manager/META-INF/context.xml
ADD tomcat-users.xml /usr/local/tomcat/conf/tomcat-users.xml
ADD root.war /usr/local/tomcat/webapps/demo.war
